#include "voronoi_device.cuh"

// This is a recursive variant, keep recursion limits and stack size constraints in mind:
__device__
void search_2d_tree(const point_2d_t *tree, const uint32_t tree_size,
                    const point_2d_t point, uint32_t depth, point_2d_t *current_best, uint32_t *best_dist,
                    uint32_t curr_idx) {

    point_2d_t curr_node = tree[curr_idx];

    uint8_t axis = (depth % 2);
    uint32_t curr_distance =
            (curr_node.x - point.x) * (curr_node.x - point.x) + (curr_node.y - point.y) * (curr_node.y - point.y);

    // Needed later for the upward traversal:
    uint32_t curr_split_dist = axis == 0 ? (curr_node.x - point.x) : (curr_node.y - point.y);

    if (curr_distance < *best_dist) {
        *best_dist = curr_distance;
        *current_best = curr_node;
    }

    // Trying to keep divergence to a minimum, we setup all the indices we might need beforehand:
    uint32_t direction = axis == 0 ? curr_node.x > point.x : curr_node.y > point.y;
    uint32_t next_idx = direction ? 2 * curr_idx + 1 : 2 * curr_idx + 2;
    uint32_t other_idx = direction ? 2 * curr_idx + 2 : 2 * curr_idx + 1;

    // Traverse down the tree (there cannot be a random point on 0, 0 and the tree was constructed with
    // a clear allocation, so a 0,0 signals an empty node):
    if (next_idx < tree_size && !(tree[next_idx].x == 0 && tree[next_idx].y == 0)) {
        search_2d_tree(tree, tree_size, point, depth + 1, current_best, best_dist, next_idx);
    }

    // Check on the upwards path, if there might be a closer point in the other side of the splitting line:
    if (curr_split_dist * curr_split_dist < *best_dist && !(tree[other_idx].x == 0 && tree[other_idx].y == 0)) {
        search_2d_tree(tree, tree_size, point, depth + 1, current_best, best_dist, other_idx);
    }

}

__global__
void voronoi_kernel(const point_2d_t *tree, const uint32_t tree_size,
                    const uint8_t *input, uint8_t *output,
                    uint32_t image_width, uint32_t image_height, uint32_t channels) {

    uint32_t global_x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint32_t global_y = (blockIdx.y * blockDim.y) + threadIdx.y;
    uint32_t global_pixel_index = (global_y * image_width + global_x) * channels;

    point_2d_t nearest_neighbor = {0, 0};
    uint32_t distance = 0xffffffff;
    search_2d_tree(tree, tree_size, {global_x, global_y}, 0, &nearest_neighbor, &distance, 0);
    int32_t nn_idx = (nearest_neighbor.y * image_width + nearest_neighbor.x) * channels;

    // Add some luminance variance:
    int multiplier = (nn_idx % 2) - 1;
    int variance = (nn_idx % 10) * multiplier * 2;

    // If imagesize % blocksize != 0, we have to check:
    if (global_pixel_index < image_width * image_height * channels) {
        output[global_pixel_index + 0] = min(max(input[nn_idx + 0] + variance, 0), 255);
        output[global_pixel_index + 1] = min(max(input[nn_idx + 1] + variance, 0), 255);
        output[global_pixel_index + 2] = min(max(input[nn_idx + 2] + variance, 0), 255);
    }
}