#ifndef CUDA_VORONOI_LIBPPM_WRAPPER_HPP
#define CUDA_VORONOI_LIBPPM_WRAPPER_HPP

#include <cstdint>
#include <string>
#include <vector>
#include "libppm/libppm.h"

/**
 * Loads the image at the given path into the vector in a RGB-pattern
 */
void loadImage(std::vector<uint8_t> &image_source,
               size_t &image_width,
               size_t &image_height,
               size_t channels,
               const std::string &input_path) {

    PPM_Image *image = loadPPM(input_path.c_str());
    image_width = image->image_width;
    image_height = image->image_height;
    image_source = std::vector<uint8_t>(image->data, image->data + image_width * image_height * channels);
    deletePPM(image);
}


/**
 * Saves the image at the given path into the vector in a RGB-pattern
 */
void saveImage(const uint8_t *image_source,
               const size_t image_width,
               const size_t image_height,
               const std::string &output_path) {

    PPM_Image *image = createPPM(P6, image_width, image_height, 255, (unsigned char *) image_source);
    savePPM(output_path.c_str(), image);
    deletePPM(image);

}

#endif //CUDA_VORONOI_LIBPPM_WRAPPER_HPP