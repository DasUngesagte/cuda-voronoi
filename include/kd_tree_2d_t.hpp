#ifndef CUDA_VORONOI_KD_TREE_2D_T_HPP
#define CUDA_VORONOI_KD_TREE_2D_T_HPP

#include <vector>
#include "point_2d_t.hpp"

typedef struct kd_node_t {
    point_2d_t median;
    struct kd_node_t *left_tree;
    struct kd_node_t *right_tree;
} kd_node_t;

kd_node_t *construct_tree(std::vector<point_2d_t> &points);

point_2d_t *construct_tree_flat(std::vector<point_2d_t> &points, uint32_t &tree_size);

void delete_tree(kd_node_t *root);

void delete_tree(point_2d_t *root);

#endif //CUDA_VORONOI_KD_TREE_2D_T_HPP