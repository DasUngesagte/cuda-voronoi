#include <algorithm>
#include <cstdlib>
#include <cmath>
#include "kd_tree_2d_t.hpp"

#define K 2

kd_node_t *construct_recursion(std::vector<point_2d_t> &points, unsigned depth) {
    if (points.empty()) { return nullptr; }
    unsigned axis = depth % K;

    // Sort point list....
    if (axis == 0)
        std::sort(points.begin(), points.end(), cmp_x);
    else
        std::sort(points.begin(), points.end(), cmp_y);

    size_t median_idx = points.size() / 2;
    point_2d_t median_point = points[median_idx];

    auto current = (kd_node_t *) malloc(sizeof(kd_node_t));
    current->median = median_point;
    std::vector<point_2d_t> left_points(points.begin(), points.begin() + median_idx);
    current->left_tree = construct_recursion(left_points, depth + 1);
    std::vector<point_2d_t> right_points(points.begin() + median_idx + 1, points.end());
    current->right_tree = construct_recursion(right_points, depth + 1);

    return current;
}

kd_node_t *construct_tree(std::vector<point_2d_t> &points) {
    return construct_recursion(points, 0);
}

void construct_recursion_flat(point_2d_t *tree, size_t index, std::vector<point_2d_t> &points, unsigned depth) {
    if (points.empty()) { return; }
    unsigned axis = depth % K;

    // Sort point list....
    if (axis == 0)
        std::sort(points.begin(), points.end(), cmp_x);
    else
        std::sort(points.begin(), points.end(), cmp_y);

    size_t median_idx = points.size() / 2;
    point_2d_t median_point = points[median_idx];

    tree[index] = median_point;
    std::vector<point_2d_t> left_points(points.begin(), points.begin() + median_idx);
    construct_recursion_flat(tree, 2 * index + 1, left_points, depth + 1);
    std::vector<point_2d_t> right_points(points.begin() + median_idx + 1, points.end());
    construct_recursion_flat(tree, 2 * index + 2, right_points, depth + 1);

}

point_2d_t *construct_tree_flat(std::vector<point_2d_t> &points, uint32_t &tree_size) {
    tree_size = pow(2 , std::ceil(log2f(points.size())));
    auto tree = (point_2d_t *) std::calloc(tree_size, sizeof(point_2d_t));
    construct_recursion_flat(tree, 0, points, 0);
    return tree;
}

void delete_tree(kd_node_t *root) {
    if (!root->left_tree) delete_tree(root->left_tree);
    if (!root->right_tree) delete_tree(root->right_tree);
    std::free(root);
}

void delete_tree(point_2d_t *root) {
    std::free(root);
}