#ifndef CUDA_VORONOI_POINT_2D_T_HPP
#define CUDA_VORONOI_POINT_2D_T_HPP

#include <cstdint>

typedef struct {
    uint32_t x, y;
} point_2d_t;

struct {
    bool operator()(point_2d_t a, point_2d_t b) const {
        return a.x < b.x;
    }
} cmp_x;

struct {
    bool operator()(point_2d_t a, point_2d_t b) const {
        return a.y < b.y;
    }
} cmp_y;

#endif //CUDA_VORONOI_POINT_2D_T_HPP