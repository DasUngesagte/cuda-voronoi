#include <iostream>
#include <cstdlib>
#include <chrono>
#include <vector>
#include <random>

#include "libppm/libppm.h"
#include "include/point_2d_t.hpp"
#include "include/kd_tree_2d_t.hpp"

#include "libppm_wrapper.hpp"
#include "voronoi_device.cuh"

// Number of channels in a given image, 3 for RGB:
#define CHANNELS 3

// Error checking for CUDA:
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }

/**********************************************************************************************************************
 *                                                 HELPER                                                             *
 **********************************************************************************************************************/

inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true) {
    if (code != cudaSuccess) {
        fprintf(stderr, "CUDA Error: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

void random_points(std::vector<point_2d_t> &points, size_t no_points, size_t image_width, size_t image_height) {
    std::random_device random_device;
    std::mt19937 twister(random_device());
    // Create random points, but not on the edges:
    std::uniform_int_distribution<uint32_t> distr_x(1, image_width - 1);
    std::uniform_int_distribution<uint32_t> distr_y(1, image_height - 1);
    for (unsigned i = 0; i < no_points; ++i) {
        points[i] = {distr_x(twister), distr_y(twister)};
    }
}

uint8_t *voronoi(std::vector<uint8_t> source_image, size_t image_width, size_t image_height, point_2d_t *tree,
                 size_t tree_size, int verbose) {

    size_t image_size = CHANNELS * image_height * image_width;
    point_2d_t *tree_dev;
    gpuErrchk(cudaMalloc(&tree_dev, tree_size * sizeof(point_2d_t)));
    gpuErrchk(cudaMemcpy(tree_dev, tree, tree_size * sizeof(point_2d_t), cudaMemcpyHostToDevice));

    uint8_t *source_image_gpu;
    gpuErrchk(cudaMalloc(&source_image_gpu, image_size * sizeof(uint8_t)));
    uint8_t *target_image_gpu;
    gpuErrchk(cudaMalloc(&target_image_gpu, image_size * sizeof(uint8_t)));
    cudaMemcpy(source_image_gpu, &source_image[0], image_size * sizeof(uint8_t), cudaMemcpyHostToDevice);

    // Kernel stuff
    dim3 threadsPerBlock(8, 4);
    dim3 numBlocks(std::ceil(1.0 * image_width / threadsPerBlock.x),
                   std::ceil(1.0 * image_height / threadsPerBlock.y));

    cudaEvent_t start_kernel, stop_kernel;
    if (verbose) {
        cudaEventCreate(&start_kernel);
        cudaEventCreate(&stop_kernel);
        cudaEventRecord(start_kernel);
    }
    voronoi_kernel<<<numBlocks, threadsPerBlock>>>(tree_dev, tree_size, source_image_gpu, target_image_gpu,
                                                   image_width, image_height, CHANNELS);

    gpuErrchk(cudaGetLastError());
    if (verbose) {
        cudaEventRecord(stop_kernel);
        cudaEventSynchronize(stop_kernel);
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start_kernel, stop_kernel);
        std::cout << "Kernel finished in: \t " << milliseconds << " ms" << std::endl;
    }

    auto target_image = new uint8_t[image_size];
    gpuErrchk(cudaMemcpy(target_image, target_image_gpu, image_size * sizeof(uint8_t), cudaMemcpyDeviceToHost));

    cudaFree(tree_dev);
    cudaFree(source_image_gpu);
    cudaFree(target_image_gpu);
    return target_image;
}

/**********************************************************************************************************************
 *                                                  MAIN                                                              *
 **********************************************************************************************************************/

int main(int argc, char **argv) {

    if (argc < 4) {
        std::cout << "Usage:" << std::endl;
        std::cout << argv[0] << " input output number_of_regions" << std::endl;
        std::cout << "Append -v for verbose output." << std::endl;
        return 1;
    }
    int verbose = (argc > 4 && std::string(argv[4]) == std::string("-v"));

    // Device limits:
    int value;
    cudaDeviceGetAttribute(&value, cudaDevAttrMaxSharedMemoryPerBlock, 0);
    if (verbose) std::cout << "SMEM available: \t" << value << " bytes" << std::endl;
    size_t pValue;
    cudaDeviceSetLimit(cudaLimitStackSize, value);
    cudaDeviceGetLimit(&pValue, cudaLimitStackSize);
    if (verbose) std::cout << "Stack size limit: \t" << pValue << " bytes" << std::endl;

    // Load image:
    std::string input_path = argv[1];
    std::string output_path = argv[2];
    std::vector<uint8_t> source_image;
    size_t image_width;
    size_t image_height;
    loadImage(source_image, image_width, image_height, CHANNELS, input_path);
    if (verbose) std::cout << "\nImage loaded." << std::endl;

    // Create random points:
    uint no_points = std::strtol(argv[3], nullptr, 10);
    std::vector<point_2d_t> points(no_points);
    random_points(points, no_points, image_width, image_height);
    if (verbose) std::cout << points.size() << " random points created.\n" << std::endl;

    // Construct 2dim k-d-tree for space partitioned nn-search:
    auto start = std::chrono::high_resolution_clock::now();
    uint32_t tree_size;
    point_2d_t *tree = construct_tree_flat(points, tree_size);
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    if (verbose) std::cout << "2-d tree created in: \t " << duration.count() / 1000.0 << " ms" << std::endl;

    // Start device code:
    uint8_t *target_image = voronoi(source_image, image_width, image_height, tree, tree_size, verbose);

    // Save image:
    if (verbose) std::cout << "\nSaving image..." << std::endl;
    saveImage(target_image, image_width, image_height, output_path);
    if (verbose) std::cout << "Done!" << std::endl;

    delete_tree(tree);
    delete[] target_image;

    return 0;
}