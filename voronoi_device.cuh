#ifndef CUDA_VORONOI_VORONOI_DEVICE_CUH
#define CUDA_VORONOI_VORONOI_DEVICE_CUH

#include "include/point_2d_t.hpp"

__global__
void voronoi_kernel(const point_2d_t *tree, uint32_t tree_size,
                    const uint8_t *input, uint8_t *output,
                    uint32_t image_width, uint32_t image_height, uint32_t channels);

__device__
void search_2d_tree(const point_2d_t *tree, uint32_t tree_size,
                    point_2d_t point, uint32_t depth, point_2d_t *current_best, uint32_t *best_dist,
                    uint32_t curr_idx);

#endif //CUDA_VORONOI_VORONOI_DEVICE_CUH